package test;

import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLObjectRenderer;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.util.SimpleIRIMapper;
import uk.ac.manchester.cs.owlapi.dlsyntax.DLSyntaxObjectRenderer;

import java.io.File;

/**
 * Created by haixiao on 2015/12/28.
 * Email: wumo@outlook.com
 */
public class CreateOWL {
	private static OWLObjectRenderer renderer = new DLSyntaxObjectRenderer();

	public static void main(String[] args) throws OWLOntologyCreationException, OWLOntologyStorageException {
		//import multiple ontology files
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		String namespace = "http://warfare/owl/";
		String rootOWL = "http://warfare/owl/test3";
		File owlDirectory = new File("owl");
		if (owlDirectory.exists() && owlDirectory.isDirectory()) {
			File[] files = owlDirectory.listFiles();
			if (files != null) {
				IRI[] iris = new IRI[files.length];
				for (int i = 0; i < files.length; i++) {
					iris[i] = IRI.create(namespace + files[i].getName());
					manager.addIRIMapper(new SimpleIRIMapper(iris[i], IRI.create(files[i])));
				}
			}
		}

		OWLOntology ontology = manager.loadOntology(IRI.create(rootOWL));
		System.out.println("Loaded ontology: " + ontology);
		System.out.println(" from: " + manager.getOntologyDocumentIRI(ontology));

		OWLDataFactory factory = manager.getOWLDataFactory();
		OWLClass thing = factory.getOWLThing();
		OWLReasonerFactory reasonerFactory = PelletReasonerFactory.getInstance();
		OWLReasoner reasoner = reasonerFactory.createReasoner(ontology, new SimpleConfiguration());
		for (Node<OWLClass> classes : reasoner.getSubClasses(thing, false)) {
			for (OWLClass aClass : classes.getEntities()) {
				System.out.println("Class: " + renderer.render(aClass));
				System.out.println();
			}
		}

		manager.saveOntology(ontology, System.out);
	}
}
